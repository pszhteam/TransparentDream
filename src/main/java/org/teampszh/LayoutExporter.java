package org.teampszh;


import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptorFactory;
import com.intellij.openapi.fileChooser.FileChooserFactory;
import com.intellij.openapi.fileChooser.FileSaverDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileWrapper;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.openapi.wm.impl.DesktopLayout;
import com.intellij.openapi.wm.impl.ToolWindowManagerImpl;


import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import java.io.File;
import java.util.logging.LogRecord;

import static com.intellij.structuralsearch.impl.matcher.PatternTreeContext.File;

public class LayoutExporter extends AnAction {
    @Override
    public void actionPerformed(AnActionEvent e) {
        ToolWindowManagerImpl manager = (ToolWindowManagerImpl) ToolWindowManager.getInstance(e.getProject());
        DesktopLayout dl = manager.getLayout();
        Element layout = dl.writeExternal("layout");

        // Use XML to covert Element to String
        XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
        String exportContent = outputter.outputString(layout);

        // Describe file saving dialog
        FileSaverDescriptor descriptor = new FileSaverDescriptor(
                "Save Layout",
                "Choose path for layout export...",
                "xml");

        // Obtain a file wrapper from FileChooserFactory
        VirtualFileWrapper wrapper = FileChooserFactory
                .getInstance()
                .createSaveFileDialog(descriptor, e.getProject())
                .save(e.getProject().getBaseDir(), "layout.xml");

        if (wrapper == null) return;
        VirtualFile file = wrapper.getVirtualFile(true);

        if (file == null) try {
            throw new Exception("Couldn't create new file");
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        // Write to disk outside of main GUI thread
        new WriteCommandAction.Simple(e.getProject()) {
            @Override
            protected void run() throws Throwable {
                VfsUtil.saveText(file, exportContent);
            }
        }.execute();


        FileChooser.chooseFile(
                FileChooserDescriptorFactory.createSingleFileDescriptor(),
                e.getProject(),
                null,
                newFile -> importLayoutFileToProject(file.getCanonicalPath(), e.getProject())
        );
        Notifications.Bus.notify(new Notification(
                "Preserve Layout",
                "Successful Export",
                "Saved to " + file.getCanonicalPath(),
                NotificationType.INFORMATION
        ), e.getProject());

        Notifications.Bus.notify(new Notification(
                "Preserve Layout",
                "Export Failed",
               "export failed",

                NotificationType.ERROR
        ), e.getProject());


    }
    @Override
    public void update(AnActionEvent e) {
        super.update(e);
     //   e.getPresentation().setIcon(AllIcons.Ide.Info_notifications);
    }

    private void importLayoutFileToProject(String path, Project project) {
        if (path == null) return;
        if (project == null) return;

        try {
            Element layout = parseLayoutFile(path);
            applyLayoutToProject(layout, project);
        } catch (Exception ex) {
            // notify user
        }
    }

    private Element parseLayoutFile(String path) throws Exception {
        Document doc = new SAXBuilder().build(new File(path));
        return doc.getRootElement();
    }

    private void applyLayoutToProject(Element layout, Project project) {
        ToolWindowManagerImpl mgr = (ToolWindowManagerImpl) ToolWindowManager.getInstance(project);
        DesktopLayout dl = mgr.getLayout();
        dl.readExternal(layout);
    }
}
